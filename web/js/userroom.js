var id_db = "";
$(document).ready(function(){
  var idTicket = 0;
  var expreg = /(usrid=)(\d+)/;
  var expreg2 = /(usrg=)(\w+)/;
  var id = myCookies.match(expreg);
  var groupUser = myCookies.match(expreg2);
  var staffOnline;

  $.ajax({
    url: '/../proyectoDAW/app/getConversations.php',
    method: 'POST',
    context: document.body,
    data: {id: id[2], group: groupUser[2]}
  })
  .done(function(data) {
    data = JSON.parse(data);
    console.log(data);
    if(data.data == "INTERNAL_ERROR"){
      //DO NOTHING
    }else{
      var idStaff = 0;
      var lastMessage = data.data[data.data.length-1].content;
      var timeLastMessage = data.data[data.data.length-1].time_message;
      for(var i = 0;i<data.data.length;i++) {
        if(data.data[i].id_emissor_staff){
          idStaff = data.data[i].id_emissor_staff;
        }
      }
      if($('.show-indication > span').html() !== ""){
        $('.show-indication').addClass('hidden');
      }
      $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+idStaff+'">'+
                                                  '<div class="con-left-img"></div>'+
                                                  '<div class="con-name-message">'+
                                                    '<span class="con-name-label">Support Member'+
                                                    '</span></br>'+
                                                    '<span class="con-message-label">'+
                                                      lastMessage+
                                                      '</span>'+
                                                  '</div>'+
                                                  '<div class="con-right-date">'+
                                                    '<span class="date">'+timeLastMessage+'</span>'+
                                                  '</div>'+
                                                 '</div>');
      for(var i = 0;i<data.data.length;i++){
        if(data.data[i].id_emissor_user != null){
          $('.content-messages-chat').append('<div class="col-xs-12">'+
                                              '<span class="speech">'+
                                                data.data[i].content+
                                                '<div class="timestamp pull-right">'+
                                                  data.data[i].time_message+
                                                  '<span class="material-icons chat-check">'+
                                                    'done_all'+
                                                  '</span>'+
                                                '</div>'+
                                              '</span>'+
                                             '</div>');
        }else{
          $('.content-messages-chat').append('<div class="col-xs-12">'+
                                              '<span class="speech-other">'+
                                                data.data[i].content+
                                                '<div class="timestamp pull-right">'+
                                                  data.data[i].time_message+
                                                  '<span class="material-icons chat-check">'+
                                                    'done_all'+
                                                  '</span>'+
                                                '</div>'+
                                              '</span>'+
                                             '</div>');
        }
        $('.colum-chat').scrollTop($('.content-messages-chat').height());
      }
      socket.emit('set_ticket_user', {id:id[2], ticket_id:data.data[0].id_ticket});
    }
  });

  $('.input-chat').html("");
  $('.input-chat').focus();
  $(document.body).on('keydown', '.input-chat', function(event) {
    if(event.keyCode==13) {
      event.preventDefault();
      if($('.show-indication > span').html() !== ""){
        $('.show-indication').addClass('hidden');
      }
      if(staffOnline === 0){
        $('.show-indication').html('En estos momentos no hay disponible ningún miembro de soporte');
        $('.content-messages-chat').addClass('hidden');
        $('.show-indication').removeClass('hidden');
      }else{
        if($(this).val()!='' && $(this).val()!=null && $(this).val()!=undefined) {
          var message = $('.input-chat').val();
          $(this).val("");
          var f = new Date();
          var date = formatDate(f);
          $('.content-messages-chat').append('<div class="col-xs-12">'+
                                              '<span class="speech">'+
                                                message+
                                                '<div class="timestamp pull-right">'+
                                                  date+
                                                  '<span class="material-icons chat-check">'+
                                                    'done_all'+
                                                  '</span>'+
                                                '</div>'+
                                              '</span>'+
                                             '</div>');
          console.log(idTicket);
          if($('.conversation').attr('data-id') == 0){
            socket.emit('new_message', { id: socket.id, message: message, group: 'user', id_db: id[2], date_message: date });
          }else{
            socket.emit('new_message', { id: socket.id, message: message, group: 'user', id_db: id[2], date_message: date,to:$('.conversation').attr('data-id')});
          }
          $('.colum-chat').scrollTop($('.content-messages-chat').height());
        }
        if($('.content-recent-conversations').children().length === 0){
          var lastMessage = $('.content-messages-chat:last-child>.col-xs-12>span')[0].childNodes[0].data;
          var lastDateMessage = $('.content-messages-chat:last-child>.col-xs-12>span>.timestamp')[0].childNodes[0].data;
          $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+idTicket+'">'+
                                                      '<div class="con-left-img"></div>'+
                                                      '<div class="con-name-message">'+
                                                        '<span class="con-name-label">Support Member'+
                                                        '</span></br>'+
                                                        '<span class="con-message-label">'+
                                                          lastMessage+
                                                          '</span>'+
                                                      '</div>'+
                                                      '<div class="con-right-date">'+
                                                        '<span class="date">'+lastDateMessage+'</span>'+
                                                      '</div>'+
                                                     '</div>');
        }else{
          console.log("entra");
          $('.content-recent-conversations').children().remove();
          var lastMessage = $('.content-messages-chat>.col-xs-12:last-child>span')[0].childNodes[0].data;
          var lastDateMessage = $('.content-messages-chat>.col-xs-12:last-child>span>.timestamp')[0].childNodes[0].data;
          // Juanra
          $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+idTicket+'">'+
                                                      '<div class="con-left-img"></div>'+
                                                      '<div class="con-name-message">'+
                                                        '<span class="con-name-label">Support Member'+
                                                        '</span></br>'+
                                                        '<span class="con-message-label">'+
                                                          lastMessage+
                                                          '</span>'+
                                                      '</div>'+
                                                      '<div class="con-right-date">'+
                                                        '<span class="date">'+lastDateMessage+'</span>'+
                                                      '</div>'+
                                                     '</div>');
        }
      }
    }
  });

  socket.on('new_message', function(data){
    console.log(data);
    idTicket = data.id_db;
      if($('.content-recent-conversations').length !== 0){
        $('.conversation').remove();
        $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+data.id_db+'">'+
                                                      '<div class="con-left-img"></div>'+
                                                      '<div class="con-name-message">'+
                                                        '<span class="con-name-label">Support Member'+
                                                        '</span></br>'+
                                                        '<span class="con-message-label">'+
                                                          data.message+
                                                        '</span>'+
                                                      '</div>'+
                                                      '<div class="con-right-date">'+
                                                        '<span class="date">'+data.date_message+'</span>'+
                                                      '</div>'+
                                                    '</div>');
        $('.conversation').attr('data-id',data.id_db);
      }
      $('.content-messages-chat').attr('data-id',data.id_db);
      $('.content-messages-chat').append('<div class="col-xs-12">'+
                                            '<span class="speech-other">'+
                                              data.message+
                                              '<div class="timestamp pull-right">'+
                                                data.date_message+
                                                '<span class="material-icons chat-check">'+
                                                  'done_all'+
                                                '</span>'+
                                              '</div>'+
                                            '</span>'+
                                          '</div>');
    // }
    $('.colum-chat').scrollTop($('.content-messages-chat').height());
  });

  $(document.body).on('click','.send-message', function(){
    var e = $.Event("keydown", {keyCode: 13});
    $('.input-chat').trigger(e);
  });
  socket.on('del-chat', function(){
    $('.conversation').html("");
    $('.content-messages-chat').html("");
    $('.show-indication').removeClass('hidden');
    $( ".input-chat" ).prop( "disabled", true );
    $('.show-indication > span').html("Su consulta ha finalizado, esperamos haber solucionado tu problema. </br> Un saludo!");
  });

  socket.on('server_new_connection',function(data){
    console.log("entra");
    console.log(data.staff.length);
    staffOnline = data.staff.length;
    if(staffOnline.length !== 0){
      $('.show-indication').addClass('hidden');
      $('.content-messages-chat').removeClass("hidden");
    }
  });
});

function formatDate(date){
  var minutes = date.getMinutes();
  var hours = date.getHours();
  if(minutes < 10){
    minutes = "0"+minutes;
  }
  if(hours < 10){
    hours = "0"+hours;
  }
  return hours+":"+minutes;
}
