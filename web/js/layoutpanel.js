$(document).ready(function(){
  console.log(Cookies.get('usrid'));
  if(Cookies.get('usrid') != undefined){
    $('.content-log-out').removeClass('hidden');
  }else {
    $('.content-log-out').addClass('hidden');
  }
  $('.exit').click(function() {
    Cookies.remove('usrg', {path: '/'});
    Cookies.remove('usrid', {path: '/'});
    Cookies.remove('usrp', {path: '/'});
    window.location.href = 'index.php?ctl=landing';
  });
});
