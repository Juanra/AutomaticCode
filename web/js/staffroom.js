$(document).ready(function(){
  var expreg = /(usrid=)(\d+)/;
  var expreg2 = /(usrg=)(\w+)/;
  var id = myCookies.match(expreg);
  var groupUser = myCookies.match(expreg2);
  var idTicket = 0;
  console.log(id[2]);

  $.ajax({
    url: '/../proyectoDAW/app/getConversations.php',
    method: 'POST',
    context: document.body,
    data: {id: id[2], group: groupUser[2]}
  })
  .done(function(data) {
    data = JSON.parse(data);
    if(data.data == "INTERNAL_ERROR"){
      //DO NOTHING
    }else{
      $( ".input-chat" ).prop( "disabled", false );
      for(var i = 0;i<data.data.length;i++){
        var lastMessage = data.data[i].content;
        var timeLastMessage = data.data[i].time_message;
        $('.content-recent-conversations').prepend('<div class="conversation" ticket-id="'+data.data[i].id_ticket+'">'+
        '<div class="con-left-img"></div>'+
        '<div class="con-name-message">'+
        '<span class="con-name-label">'+data.data[i].username+
        '</span></br>'+
        '<span class="con-message-label">'+
        lastMessage+
        '</span>'+
        '</div>'+
        '<div class="con-right-date">'+
        '<span class="date">'+timeLastMessage+'</span>'+
        '</div>'+
        '</div>');
      }

      $.ajax({
        url: '/../proyectoDAW/app/getMessages.php',
        method: 'POST',
        context: document.body,
        data: {id: data.data[0].id_ticket, group: groupUser[2]}
      })
      .done(function(data) {
        data = JSON.parse(data);
        if(data.data == "INTERNAL_ERROR"){

        }else{
          for(var i = 0;i<data.data.length;i++){
            if(data.data[i].id_emissor_staff != null){
              $('.content-messages-chat').append('<div class="col-xs-12">'+
              '<span class="speech">'+
              data.data[i].content+
              '<div class="timestamp pull-right">'+
              data.data[i].time_message+
              '<span class="material-icons chat-check">'+
              'done_all'+
              '</span>'+
              '</div>'+
              '</span>'+
              '</div>');
            }else{
              console.log($('.conversation[data-id="'+data.data[i].id_ticket+'"]'));
              $('.content-messages-chat').attr('data-id',data.data[i].id_emissor_user);
              $('.conversation[ticket-id="'+data.data[i].id_ticket+'"]').attr('data-id',data.data[i].id_emissor_user);
              $('.content-messages-chat').append('<div class="col-xs-12">'+
              '<span class="speech-other">'+
              data.data[i].content+
              '<div class="timestamp pull-right">'+
              data.data[i].time_message+
              '<span class="material-icons chat-check">'+
              'done_all'+
              '</span>'+
              '</div>'+
              '</span>'+
              '</div>');
            }
            $('.colum-chat').scrollTop($('.content-messages-chat').height());
          }
        }
      });
    }
  });

  $( ".input-chat" ).prop( "disabled", true );
  socket.on('new_message', function(data) {
    var idUser = data.id_db;
    console.log(data);
    var message = data.message;
    var date = data.date_message;
    $.ajax({
      url: '/../proyectoDAW/web/index.php?ctl=userroom',
      method: 'POST',
      context: document.body,
      data: {id: data.id_db}
    }).done(function(data) {
      var flag = false;
      data = JSON.parse(data);
      console.log(data);
      console.log($('.content-recent-conversations').children().length);
      $( ".input-chat" ).prop( "disabled", false );
      console.log($('.content-recent-conversations').children().length);
      if($('.content-recent-conversations').children().length !== 0) {
        console.log("entra por que content-recent-conversations tiene conversaciones");
        //Si la conversacion existe cambiar la hora y el mensaje a mostrar en recentConversations
        //Añadir el mensaje en la conversacion.
        $('.conversation').each(function(index, el) {
          console.log(el);
          if($(el).attr('data-id') == data.id){
            console.log("recorre conversaciones y encuentra data-id de .conversation que coincide con data.id del mensaje");
            flag = true;
            $('.conversation[data-id="'+data.id+'"]').find('.con-message-label').html(message);
            $('.conversation[data-id="'+data.id+'"]').find('.date').html(date);
            $('.selected').attr('data-id');
            if($('.content-messages-chat').attr('data-id') == idUser){
              $('.content-messages-chat').append('<div class="col-xs-12">'+
              '<span class="speech-other">'+
              message+
              '<div class="timestamp pull-right">'+
              date+
              '<span class="material-icons chat-check">'+
              'done_all'+
              '</span>'+
              '</div>'+
              '</span>'+
              '</div>');
            }
            // fnChangeConversations();
          }
        });
        // for(var i = 0;i<$('.conversation').length;i++){
        // }
        if(!flag) {
          $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+idUser+'">'+
                                                        '<div class="conv-accepted hidden"><span>RESPONDIDA</span></div>'+
                                                        '<div class="con-left-img"></div>'+
                                                        '<div class="con-name-message">'+
                                                          '<span class="con-name-label">'+
                                                            data.username+
                                                          '</span></br>'+
                                                          '<span class="con-message-label">'+
                                                            message+
                                                          '</span>'+
                                                        '</div>'+
                                                        '<div class="con-right-date">'+
                                                          '<span class="date">'+date+'</span>'+
                                                          '<span class="material-icons rm-chat" id-rm="'+data.id+'">clear</span>'+
                                                        '</div>'+
                                                      '</div>');
          $('.content-messages-chat').children().remove();
          $('.content-messages-chat').attr('data-id',data.id);
          $('.content-messages-chat').append('<div class="col-xs-12">'+
                                                '<span class="speech-other">'+
                                                  message+
                                                  '<div class="timestamp pull-right">'+
                                                    date+
                                                    '<span class="material-icons chat-check">'+
                                                      'done_all'+
                                                    '</span>'+
                                                  '</div>'+
                                                '</span>'+
                                              '</div>');
        }
      }else {
        $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+idUser+'">'+
                                                      '<div class="conv-accepted hidden"><span>RESPONDIDA</span></div>'+
                                                      '<div class="con-left-img"></div>'+
                                                      '<div class="con-name-message">'+
                                                        '<span class="con-name-label">'+
                                                          data.username+
                                                        '</span></br>'+
                                                        '<span class="con-message-label">'+
                                                          message+
                                                        '</span>'+
                                                      '</div>'+
                                                      '<div class="con-right-date">'+
                                                        '<span class="date">'+date+'</span>'+
                                                        '<span class="material-icons rm-chat" id-rm="'+data.id+'">clear</span>'+
                                                      '</div>'+
                                                    '</div>');
        $('.content-messages-chat').attr('data-id',data.id);
        $('.content-messages-chat').append('<div class="col-xs-12">'+
                                              '<span class="speech-other">'+
                                                message+
                                                '<div class="timestamp pull-right">'+
                                                  date+
                                                  '<span class="material-icons chat-check">'+
                                                    'done_all'+
                                                  '</span>'+
                                                '</div>'+
                                              '</span>'+
                                            '</div>');
      }
      $('.colum-chat').scrollTop($('.content-messages-chat').height());
    });
  });

  $('.input-chat').html("");
  $('.input-chat').focus();
  var id_to;
  $(document.body).on('keydown', '.input-chat', function(event) {
    if(event.keyCode==13) {
      event.preventDefault();
      if($('.show-indication > span').html() !== ""){
        $('.show-indication').addClass('hidden');
      }
      if($(this).val()!='' && $(this).val()!=null && $(this).val()!=undefined) {
        var message = $('.input-chat').val();
        $(this).val("");
        var f = new Date();
        var date = formatDate(f);
        id_to = $('.content-messages-chat').attr('data-id');
        $('.content-messages-chat').append('<div class="col-xs-12">'+
                                            '<span class="speech">'+
                                              message+
                                              '<div class="timestamp pull-right">'+
                                                date+
                                                '<span class="material-icons chat-check">'+
                                                  'done_all'+
                                                '</span>'+
                                              '</div>'+
                                            '</span>'+
                                          '</div>');
        socket.emit('new_message', { id: socket.id, message: message, group: 'staff', id_db: id[2], date_message: date, to: id_to, ticket_id: idTicket });
        $('.colum-chat').scrollTop($('.content-messages-chat').height());
      }
    }
  });
  $(document.body).on('click','.send-message', function(){
    var e = $.Event("keydown", {keyCode: 13});
    $('.input-chat').trigger(e);
  });

  $(document.body).on('click', '.rm-chat', function(event) {
    var id_chat = $(this).attr('id-rm');
    $.ajax({
      url: '/web/index.php?ctl=userroom&id='+id_chat+'&group=staff',
      method: 'DELETE',
      context: document.body,
    })
    .done(function(data) {
      $('.conversation[data-id="'+id_chat+'"]').html("");
      $('.content-messages-chat[data-id="'+id_chat+'"]').html("");
      $( ".input-chat" ).prop( "disabled", true );
      socket.emit('del-chat', {id: id[2], to: id_to});
    });
  });
  $(document.body).on('click', '.conversation', function(e){
    // console.log($(this).attr('data-id'));
    //Juanra
    $.ajax({
      url: '/../proyectoDAW/app/getMessages.php',
      method: 'POST',
      context: document.body,
      data: {id: $(this).attr('ticket-id'), group: groupUser[2]}
    })
    .done(function(data) {
      data = JSON.parse(data);
      console.log(data);
      if(data.data == "INTERNAL_ERROR"){

      }else{
        console.log(data.data[0].id_emissor_user);
        $('.content-messages-chat').children().remove();
        for(var i = 0;i<data.data.length;i++){
          if(data.data[i].id_emissor_staff != null){
            $('.content-messages-chat').append('<div class="col-xs-12">'+
            '<span class="speech">'+
            data.data[i].content+
            '<div class="timestamp pull-right">'+
            data.data[i].time_message+
            '<span class="material-icons chat-check">'+
            'done_all'+
            '</span>'+
            '</div>'+
            '</span>'+
            '</div>');
          }else{
            $('.content-messages-chat').attr('data-id',data.data[i].id_emissor_user);
            $('.conversation[ticket-id="'+data.data[i].id_ticket+'"]').attr('data-id',data.data[i].id_emissor_user);
            $('.content-messages-chat').append('<div class="col-xs-12">'+
            '<span class="speech-other">'+
            data.data[i].content+
            '<div class="timestamp pull-right">'+
            data.data[i].time_message+
            '<span class="material-icons chat-check">'+
            'done_all'+
            '</span>'+
            '</div>'+
            '</span>'+
            '</div>');
          }
          $('.colum-chat').scrollTop($('.content-messages-chat').height());
        }
      }
    });
    //Falta borrar todo $('.content-messages-chat')
    //Hacer peticion ajax para recibir conversacion del nuevo mensaje o el pulsado
  });
});

socket.on('server_send_id_ticket', function(data){
  idTicket = data.ticket_id;
  console.log(idTicket);
});

socket.on('server_get_request_user', function(data){
  console.log(data);
  console.log($('.conversation[data-id="'+data.to+'"]').find('.conv-accepted'));
  $('.conversation[data-id="'+data.to+'"]').find('.conv-accepted').removeClass('hidden');
  $('.conversation').each(function(index, el) {
    if(el.attr('data-id') == data.to){
      $(document).off('click',$(el),function(e){
        //REMOVEEEE JUANRA
      });
    }
  });
    // $(document).unbind('click', $('.conversation[data-id="'+data.to+'"]'), function(e){
    //
    // });
});

function formatDate(date){
  var minutes = date.getMinutes();
  var hours = date.getHours();
  if(minutes < 10){
    minutes = "0"+minutes;
  }
  if(hours < 10){
    hours = "0"+hours;
  }
  return hours+":"+minutes;
}
