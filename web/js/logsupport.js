$(document).ready(function(){
  var enterUser = false;
  var enterPass = false;
  $('[data-toggle="tooltip"]').tooltip();
  $('.user-text-log').focusout(function(event) {
    if($('.user-text-log').val()==="" || $('.user-text-log').val().trim().length>10){
      if(!validateEmail($('.user-text-log').val())){
        var html="<ul class='ul-login' style='list-style-type:none'>"+
                    "<li>El campo Usuario no puede estar vacio</li>"+
                    "<li>El usuario debe contener menos de 10 caracteres</li>"+
                    "<li>Debe de ser un email valido</li>"+
                  "</ul>";
        $('.user-text-log').popover({
          placement:	'top',
          title: "",
          html:true,
          content: html,
          trigger: 'manual'
        });
        $(this).popover('show');
        setTimeout(function(){
          $('.user-text-log').popover('hide');
        },4000);
      }
    }
  });
  $('.user-text-log').click(function(event) {
    $('.user-text-log').popover('hide');
  });
  $('.password-text-log').focusout(function(event) {
    var html="<ul class='ul-login' style='list-style-type:none'>"+
                "<li>El campo Password no puede estar vacio</li>"+
                "<li>La contraseña debe contener menos de 10 caracteres</li>"+
              "</ul>";
    $('.password-text-log').popover({
      placement:	'bottom',
      title: "",
      html:true,
      content: html,
      trigger: 'manual'
    });
    if($('.password-text-log').val()==="" || $('.password-text-log').val().trim().length>10){
      $(this).popover('show');
      setTimeout(function(){
        $('.password-text-log').popover('hide');
      },4000);
    }
  });
  $('.password-text-log').click(function(event) {
    $('.password-text-log').popover('hide');
  });
  $('.user-text-log').bind('input', function(){
    if($('.user-text-log').val()==="" || $('.user-text-log').val().trim().length>10){
      if(!validateEmail($('.user-text-log').val())){
        enterUser = false;
      }else{
        enterUser = true;
      }
    }else{
      enterUser = true;
    }
    if(enterUser && enterPass){
      $('.log-support').removeClass('disabled');
      $('.log-support').removeAttr('disabled');
    }else{
      $('.log-support').addClass('disabled');
      $('.log-support').prop('disabled',true);
    }
  });
  $('.password-text-log').bind('input', function(){
    if($('.password-text-log').val()==="" || $('.password-text-log').val().length>10){
      enterPass = false;
    }else{
      enterPass = true;
    }
    if(enterUser && enterPass){
      $('.log-support').removeClass('disabled');
      $('.log-support').removeAttr('disabled');
    }else{
      $('.log-support').addClass('disabled');
      $('.log-support').prop('disabled',true);
    }
  });

/*  $('.log-support').click(function(e){
    if ($('.user-text-log').val()==="" || $('.password-text-log').val()==="") {
      e.preventDefault();
    }else{
      alert("true");
      return true;
    }
  }); */
});
var validateEmail = function(email) {
	var re=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}
