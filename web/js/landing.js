$(document).ready(function(){
  $('#section2').height($(document.body).height());
  $('#section3').height($(document.body).height());
  //for resize window
  $(window).resize(function() {
    $('#section2').height($(document.body).height());
    $('#section3').height($(document.body).height());
  });

  var flag = true;
  var eventMouseWheel = (/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel";
  $(document.body).on(eventMouseWheel ,function(event) {
    if(flag){
      flag = false;
      var heightBody;
      var active;
      if(/Firefox/i.test(navigator.userAgent)){
        //Firefox
        if(event.originalEvent.detail <= 0){
          active = document.getElementsByClassName('active');
          if(active[0].id == "section1") {
            heightBody = $(document.body).height() * 2;
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section3').addClass('active');
            $('#section1').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section2") {
            heightBody = $(document.body).height();
            $('.content-all-images').animate({top: 0});
            $('#section1').addClass('active');
            $('#section2').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section3") {
            heightBody = $(document.body).height();
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section2').addClass('active');
            $('#section3').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }
        }else{
          active = document.getElementsByClassName('active');
          if(active[0].id == "section1") {
            heightBody = $(document.body).height();
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section2').addClass('active');
            $('#section1').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section2") {
            heightBody = $(document.body).height() * 2;
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section3').addClass('active');
            $('#section2').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section3") {
            $('.content-all-images').animate({top: 0});
            $('#section1').addClass('active');
            $('#section3').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }
        }
      }else{
        //Chrome
        if(event.originalEvent.wheelDelta  >= 0){
          active = document.getElementsByClassName('active');
          if(active[0].id == "section1") {
            heightBody = $(document.body).height() * 2;
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section3').addClass('active');
            $('#section1').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section2") {
            heightBody = $(document.body).height();
            $('.content-all-images').animate({top: 0});
            $('#section1').addClass('active');
            $('#section2').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section3") {
            heightBody = $(document.body).height();
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section2').addClass('active');
            $('#section3').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }
        }else{
          active = document.getElementsByClassName('active');
          if(active[0].id == "section1") {
            heightBody = $(document.body).height();
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section2').addClass('active');
            $('#section1').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section2") {
            heightBody = $(document.body).height() * 2;
            $('.content-all-images').animate({top: "-"+heightBody+"px"});
            $('#section3').addClass('active');
            $('#section2').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }else if(active[0].id == "section3") {
            $('.content-all-images').animate({top: 0});
            $('#section1').addClass('active');
            $('#section3').removeClass('active');
            setTimeout(function(){flag = true;},400);
          }
        }
      }
    }
  });
});
