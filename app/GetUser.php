<?php
require_once __DIR__ . '/../app/Model.php';
require_once __DIR__ . '/../app/Config.php';
if(isset($_GET['id'])){
  $m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
      Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
  if($_SERVER['REQUEST_METHOD'] == 'GET'){
    $r = $m->getUser($_GET['id']);
    //echo json_encode($r);
    echo json_encode($r);
  }
}
?>
