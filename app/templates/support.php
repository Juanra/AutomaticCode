<?php ob_start()?>
  <script src="js/support.js"></script>
<?php $loadScripts = ob_get_clean() ?>
<div class="fullpage">
  <div class="content-support-page">
    <h2>Bienvenido al soporte técnico de AutomaticCode</h2>
    <i class="material-icons icon-support">question_answer</i>
    <div class="subtitle-support">¿Cómo podemos ayudarte?</div>
    <div>Recibe soporte técnico por nuestro chat online o mediante correo electrónico </div>
    <a href="index.php?ctl=logsupport">Contactar con el soporte en linea </a>
  </div>
  <!-- Modificar con router para enviar directamente a InicioSoporte
  o si existe la cookie a comenzarSoporte-->
</div>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
