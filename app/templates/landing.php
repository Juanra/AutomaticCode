<?php ob_start()?>
  <script src="js/landing.js"></script>
<?php $loadScripts = ob_get_clean() ?>
  <div id="fullpage" class="fullpage">
    <div clasS="content-all-images">
      <div class="section active" id="section1">
        <div class="content-section-1">
          <h2>Crea todo lo que puedas imaginar. En cualquier lugar.</h2>
          <div>
            El mejor software de programación de aplicaciones de escritorio y webs.
            Trabaja en tu ordenador de sobremesa y dispositivos móviles para crear y
            diseñar aplicaciones de escritorio, aplicaciones moviles, webs, videojuegos
            y mucho más.
          </div>
        </div>
      </div>
      <div class="section" id="section2">
        <div class="content-section-2">
          <h2>Consigue AutomaticCode con cualquiera de los planes.</h2>
          <div class="content-plans">
            <div class="individual-plan">
              <i class="material-icons">people</i>
              <h4>Individual</h4>
              <div>
                Consigue AutomaticCode como parte del plan para todas las aplicaciones
                por solo </br> <span>24,19 €</span> </br> al mes (IVA incluido).
              </div>
            </div>
            <div class="student-plan">
              <i class="material-icons">school</i>
              <h4>Estudiantes</h4>
              <div>
                Obtén un 74 % de descuento con las ofertas de precios para el sector educativo.
                Hazte con todas las aplicaciones de AutomaticCode por solo </br> <span>19,66 €</span> </br> al mes (IVA incluido).
              </div>
            </div>
            <div class="bussines-plan">
              <i class="material-icons">business_center</i>
              <h4>Empresas</h4>
              <div>
                Consigue AutomaticCode junto con una gestión de licencias simple y una sencilla
                implementación para equipos desde </br> <span>69,99 €</span> </br> al mes (IVA incluido).
              </div>
            </div>
          </div>
          <div class="button-select-plan">Selecciona  tu plan</div>
        </div>
      </div>
      <div class="section" id="section3">
        <div class="content-section-3">
          <h2>Una experiencia totalmente nueva en la programación.</h2>
          <div class="description-software">
            Descubre como AutomaticCode te ayuda a convertir rapidamente cualquier idea en increible
            trabajo con las fantásticas funciones de diseño web y aplicaciones de escritorio y móviles.
            </br>
            <a href="#">Ver todas las novedades</a>
          </div>
          <div class="content-examples">
            <div class="content-example">
              <div class="example-img">

              </div>
              <div class="example-info">
                Diseña tus páginas web y crea tus propias plantillas en pocos minutos
                mediante el sencillo editor visual.
              </div>
            </div>
            <div class="content-example">
              <div class="example-img">

              </div>
              <div class="example-info">
                Las herramientas fáciles de usar te ayudan a exportar tu código
                para cualquier plataforma que desees.
              </div>
            </div>
            <div class="content-example">
              <div class="example-img">

              </div>
              <div class="example-info">
                Automatiza cualquier código con la herramienta de
              </div>
            </div>
            <div class="content-example">
              <div class="example-img">

              </div>
              <div class="example-info">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
