<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Mi Repositorio Personal</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo 'css/'.Config::$mvc_vis_css ?>" />
    <link rel="stylesheet" href="css/jquery.fullPage.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Comfortaa:300&subset=latin,cyrillic-ext,greek,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,greek,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery.i18n.min.js"></script>
    <script src="https://cdn.jsdelivr.net/lodash/4.6.1/lodash.min.js"></script>

  <!--  <script src="/assets/js/typeahead/bootstrap-typeahead.min.js"></script>
    <script src="/assets/js/jquery.cookie.js"></script>
    <script src="/assets/js/jquery.cookiecuttr.js"></script> -->
    <!-- <script src="js/jquery.fullPage.min.js"></script> -->
    <script src="js/dropdowns-enhancement.js"></script>
  <!--  <script src="/assets/js/ripples.min.js"></script>
    <script src="/assets/js/material.min.js"></script> -->
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="js/js.cookie.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
    <script src="/assets/js/pace.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
    <script src="js/layoutpanel.js"></script>
    <?php echo $loadScripts ?>

  </head>
  <body>
    <div id="header" class="header">
      <div class="content-logo">
        <a class="menu-landing" href="index.php?ctl=landing"></a>
        <span class="material-icons">home</span>
      </div>
      <div class="content-menu">
        <div class="content-menu-software">
          <a class="menu-support" href="index.php?ctl=support"></a>
          <span class="material-icons">settings_applications</span><span class="software-span">Software</span>
        </div>
        <div class="content-menu-articles">
          <a class="menu-support" href="index.php?ctl=support"></a>
          <span class="material-icons">library_books</span><span class="articles-span">Artículos</span>
        </div>
        <div class="content-menu-about">
          <a class="menu-support" href="index.php?ctl=support"></a>
          <span class="material-icons">group_work</span><span class="about-span">Quiénes somos</span>
        </div>
        <div class="content-menu-faq">
          <a class="menu-support" href="index.php?ctl=support"></a>
          <span class="material-icons">help_outline</span><span class="faq-span">F.A.Q</span>
        </div>
        <div class="content-menu-support">
          <a class="menu-support" href="index.php?ctl=support"></a>
          <span class="material-icons">forum</span><span class="support-span">Support</span>
        </div>
      </div>
      <div class="content-log-out hidden">
        <span class="material-icons exit">exit_to_app</span>
      </div>
    </div>
    <div class="contenido">
			<?php echo $content ?>
		</div>
	</body>
</html>
